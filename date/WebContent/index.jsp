<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="/functions" prefix="f" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>Localized Dates</title>
  </head>
  <body bgcolor="white">
  	<jsp:useBean id="locales" class="mypkg.MyLocales" scope="application"></jsp:useBean>
    <form name="localeForm" action="index.jsp" method="post">
    	<c:set var="selectedLocaleString" value="${param.locale}" />
    	<c:set var="selectedFlag" value="${!empty selectedLocaleString}" />
    	<b>Locale :</b>
    	<select name="locale">
    		<c:forEach var="localeString" items="${locales.localeNames}">
    			<c:choose>
	    			<c:when test="${selectedFlag}">
		    			<c:choose>
		    				<c:when test="${f:equals(selectedString, localeString)}">
		    					<option selected>${localeString}</option>
		    				</c:when>
		    				<c:otherwise>
		    					<option>${localeString}</option>
		    				</c:otherwise>
		    			</c:choose>
	    			</c:when>
	    			<c:otherwise>
	    				<option>${localeString}</option>
	    			</c:otherwise>
    			</c:choose>
    		</c:forEach>
    	</select>
	   	<input type="submit" name="Submit" value="Get Date">
    </form>
    
    <c:if test="${selectedFlag}">
    	<jsp:setProperty property="selectedLocaleString" name="locales" value="${selectedLocaleString}"/>
    	<jsp:useBean id="date" class="mypkg.MyDate"></jsp:useBean>
    	<jsp:setProperty property="locale" name="date" value="${locales.selectedLocale}"/>
    	<b>Date: </b>${date.date}
    </c:if>	
  </body>
</html> 
