<%@ tag language="java" pageEncoding="ISO-8859-1"%>

<%@ attribute name="greeting" required="true" %>
<%@ attribute name="name" required="true" %>

<h2>
	<font color="red">${greeting}, ${name}!</font>
</h2>