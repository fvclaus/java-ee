<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>Hello</title>
  </head>
  <bodydy>
  	<c:set var="greeting" value="Hello" />
  	<h2>${greeting}, my name is Duke. What's your name?</h2>
  	<form method="get">
  		<input type="text" name="username" size=25 />
  		<p></p>
  		<input type="submit" value="Submit">
  		<input type="reset" value="Reset">
  	</form>
  	
  	<c:if test="${fn:length(param.username) > 0}">
  		<h:response greeting="${greeting}" name="${param.username}" />
  	</c:if>
  </body>
</html> 
